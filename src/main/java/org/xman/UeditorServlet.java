package org.xman;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xman
 */
public class UeditorServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        String content = request.getParameter("content");
        if(content == null) {
            content = "Nothing...";
        }

        String realPath = request.getRealPath("/");
        String ftlHome = realPath + "ftl/";
        FreemarkerHelper helper = new FreemarkerHelper(ftlHome);

        Map<String, Object> root = new HashMap<>();
        root.put("content", content);


        helper.createHtml(root);

        System.out.println(content);

        response.getWriter().print("successful");
    }


    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}
