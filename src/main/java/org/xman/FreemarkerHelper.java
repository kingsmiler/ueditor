package org.xman;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class FreemarkerHelper {

    private File ftlHome;

    public FreemarkerHelper(String ftlHome) {
        this.ftlHome = new File(ftlHome);
    }

    public Template getTemplate(String name) {
        try {
            //通过Freemaker的Configuration读取相应的ftl
            Version version = new Version("2.3.21");
            Configuration cfg = new Configuration(version);
            //设定去哪里读取相应的ftl模板文件
            cfg.setDirectoryForTemplateLoading(ftlHome);
            //在模板文件目录中找到名称为name的文件
            Template temp = cfg.getTemplate(name);
            return temp;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void print(String name, Map<String, Object> root) {
        try {
            //通过Template可以将模板文件输出到相应的流
            Template temp = this.getTemplate(name);
            temp.process(root, new PrintWriter(System.out));
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void createHtml(Map<String, Object> root) {

        try(FileWriter out = new FileWriter(new File(ftlHome, "article.html"))) {
            Template temp = this.getTemplate("article.ftl");
            temp.process(root, out);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }

    }

}
